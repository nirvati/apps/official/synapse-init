use rand::{CryptoRng, RngCore, SeedableRng};

fn generate_random<Rng>(rng: &mut Rng) -> String
where
    Rng: RngCore + CryptoRng,
{
    let mut buf = [0u8; 32];
    rng.fill_bytes(&mut buf);
    hex::encode(buf)
}

// Try, if it fails, sleep 1 second and continue;
macro_rules! try_fn {
    ($e:expr) => {
        match $e {
            Ok(val) => val,
            Err(e) => {
                eprintln!("Error: {}", e);
                std::thread::sleep(std::time::Duration::from_secs(1));
                continue;
            }
        }
    };
}

#[derive(serde::Deserialize)]
struct NonceData {
    nonce: String,
}

#[derive(serde::Serialize)]
struct RegisterData {
    nonce: String,
    username: String,
    displayname: String,
    password: String,
    admin: bool,
    mac: String,
}

fn main() {
    let mut rng = rand_chacha::ChaChaRng::from_entropy();
    let macaroon = generate_random(&mut rng);
    let registration_secret = generate_random(&mut rng);

    let log_tmpl = include_bytes!("log.config");
    std::fs::write("/data/log.config", log_tmpl).unwrap();

    let server_name = std::env::var("DOMAIN").unwrap();
    let pg_pw = std::env::var("POSTGRES_PASSWORD").unwrap();
    let pg_host = std::env::var("POSTGRES_HOST").unwrap();
    let config_tmpl = include_str!("homeserver.yaml.tmpl")
        .replace("{{ SERVER_NAME }}", &server_name)
        .replace("{{ POSTGRES_PASSWORD }}", &pg_pw)
        .replace("{{ POSTGRES_HOST }}", &pg_host)
        .replace("{{ REGISTRATION_SHARED_SECRET }}", &registration_secret)
        .replace("{{ MACAROON_SECRET_KEY }}", &macaroon);
    std::fs::write("/data/homeserver.yaml", config_tmpl).unwrap();

    let synapse_host = std::env::var("SYNAPSE_HOST").unwrap();
    let user = std::env::var("INITIAL_USER_NAME").unwrap();
    let password = std::env::var("INITIAL_USER_PASSWORD").unwrap();

    loop {
        // Send a request to http://{synapse_host}:8008/_synapse/admin/v1/register
        let response = try_fn!(reqwest::blocking::Client::new()
            .get(&format!(
                "http://{}:8008/_synapse/admin/v1/register",
                synapse_host
            ))
            .send());
        if !response.status().is_success() {
            eprintln!("Error: {}", response.text().unwrap());
            std::thread::sleep(std::time::Duration::from_secs(1));
            continue;
        }
        let nonce_data: NonceData = try_fn!(response.json());
        let nonce = nonce_data.nonce;
        // Message is nonce, user, password, "admin", each separated by a null byte
        let msg = format!("{}\0{}\0{}\0admin", nonce, user, password);
        let hmac_digest: [u8; hmac_sha1::SHA1_DIGEST_BYTES] =
            hmac_sha1::hmac_sha1(registration_secret.as_ref(), msg.as_bytes());
        let hmac_digest_hex = hex::encode(hmac_digest);

        let register_data = RegisterData {
            nonce,
            username: user.clone(),
            displayname: user.clone(),
            password: password.clone(),
            admin: true,
            mac: hmac_digest_hex,
        };

        let response = try_fn!(reqwest::blocking::Client::new()
            .post(&format!(
                "http://{}:8008/_synapse/admin/v1/register",
                synapse_host
            ))
            .json(&register_data)
            .send());

        if response.status().is_success() {
            println!("Created user {}", user);
            println!("{}", response.text().unwrap());
            break;
        } else {
            eprintln!("Error: {}", response.text().unwrap());
            std::thread::sleep(std::time::Duration::from_secs(1));
        }
    }
}
