server_name: "{{ SERVER_NAME }}"
pid_file: /homeserver.pid
web_client: False
soft_file_limit: 0
log_config: "/data/log.config"

listeners:
  - port: 8008
    tls: false
    bind_addresses: ['::']
    type: http
    x_forwarded: true
    resources:
      - names: [client]
        compress: true
      - names: [federation]
        compress: false

database:
  name: "psycopg2"
  args:
    user: "synapse"
    password: "{{ POSTGRES_PASSWORD }}"
    database: "synapse"
    host: "{{ POSTGRES_HOST }}"
    port: "5432"

event_cache_size: "10K"

rc_messages_per_second: 0.2
rc_message_burst_count: 10.0
federation_rc_window_size: 1000
federation_rc_sleep_limit: 10
federation_rc_sleep_delay: 500
federation_rc_reject_limit: 50
federation_rc_concurrent: 3


media_store_path: "/data/media"
max_upload_size: "50M"
max_image_pixels: "32M"
dynamic_thumbnails: false

thumbnail_sizes:
- width: 32
  height: 32
  method: crop
- width: 96
  height: 96
  method: crop
- width: 320
  height: 240
  method: scale
- width: 640
  height: 480
  method: scale
- width: 800
  height: 600
  method: scale

url_preview_enabled: False
max_spider_size: "10M"

enable_registration: "False"
registration_shared_secret: "{{ REGISTRATION_SHARED_SECRET }}"
bcrypt_rounds: 12
enable_group_creation: true

enable_metrics: False
report_stats: False

macaroon_secret_key: "{{ MACAROON_SECRET_KEY }}"

signing_key_path: "/data/signing.key"
old_signing_keys: {}
key_refresh_interval: "1d" # 1 Day.

trusted_key_servers:
  - server_name: matrix.org
    verify_keys:
      "ed25519:auto": "Noi6WqcDj0QmPxCNQqgezwTlBKrfqehY1u2FyWP9uYw"

password_config:
   enabled: true